<?php

/**
 * @file
 * Definition of views_handler_field_user_link_security_tfa.
 */

// Disable some PHPCS rules which the Views API causes us to break.
// phpcs:disable Drupal.NamingConventions.ValidClassName.StartWithCaptial,Drupal.NamingConventions.ValidClassName.NoUnderscores, Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
/**
 * Field handler to present a link to the user's Security/TFA page.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_user_link_security_tfa extends views_handler_field_user_link {

  /**
   * {@inheritdoc}
   */
  public function render_link($data, $values) {
    // Build a pseudo account object to be able to check the access.
    $account = new stdClass();
    $account->uid = $data;

    if ($data && tfa_basic_setup_access($account, 'setup own tfa')) {
      $this->options['alter']['make_link'] = TRUE;

      $text = !empty($this->options['text']) ? $this->options['text'] : t('security');

      $this->options['alter']['path'] = "user/$data/security/tfa";
      $this->options['alter']['query'] = drupal_get_destination();

      return $text;
    }
  }

}
