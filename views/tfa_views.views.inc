<?php

/**
 * @file
 * Views hook implementations for integrating the TFA suite.
 */

/**
 * Implements hook_views_data().
 */
function tfa_views_views_data() {
  $data['tfa_user_settings']['table']['group'] = t('TFA Settings');

  $data['tfa_user_settings']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'LEFT',
    ),
  );

  // UID.
  $data['tfa_user_settings']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user this setting is bound to.'),
    'relationship' => array(
      'base' => 'user',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User for TFA setting'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
  );

  // Saved.
  $data['tfa_user_settings']['saved'] = array(
    'title' => t('TFA Saved'),
    'help' => t('The date user saved the setting.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function tfa_views_views_data_alter(&$data) {
  $data['users']['security_tfa_link'] = array(
    'field' => array(
      'title' => t("Link to user Security TFA page"),
      'help' => t('Provide a simple link to the security page for the user.'),
      'handler' => 'views_handler_field_user_link_security_tfa',
    ),
  );
}
